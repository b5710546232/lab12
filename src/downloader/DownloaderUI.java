package downloader;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;
import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.border.*;

/** 
 * User Interface for URL downloader.
 */
class DownloaderUI extends JFrame {
	private static final long serialVersionUID = 1L;
	
	/** graphical components */
	private JTextField urlField;
	private JTextField outputFileField;
	private JTextArea messageArea;
	protected JProgressBar progressBar = new  JProgressBar();
	
	/** labels for buttons are also action commands */
	private static final String QUERYCOMMAND = "Query";
	private static final String DOWNLOADCOMMAND = "Get!";
	private static final String CLEARCOMMAND = "Clear";
	private static final String BROWSECOMMAND = "Browse...";
	
	/** startup message */
	private static final String STARTMESSAGE =
		"Enter a URL to download, such as http://www.yahoo.com/index.html\n" +
		"Press QUERY to check the URL.\n" +
		"Press BROWSE to choose where to save download\n" +
		"Press Get! to download.\n" ;

	private static final int ROWS = 10, COLS = 40; // size of message area
	
	/** Create a new Snail User Interface
	 * @param reader reference to URL downloader
	 */
	public DownloaderUI( ) {
		initComponents( );
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/** initialize GUI components and add to frame 
	 */
	public void initComponents() {
		this.setLocation(60,60);
		this.setTitle("Super Slow URL Downloader" );

		// action buttons
		final DownloadAction downloadAction = new DownloadAction();
		JButton downloadButton = new JButton(downloadAction);
		JButton queryButton = new JButton(new QueryAction());
		JButton clearButton = new JButton(new ClearAction());
		JButton browseButton = new JButton(new BrowseAction());
		
		// input URL field and its label

		urlField = new JTextField(COLS-12);
		JLabel urlLabel = new JLabel("URL to download: ");
		urlField.addActionListener( downloadAction );
		
		// output filename field and its label

		outputFileField = new JTextField(COLS-12);
		JLabel outLabel = new JLabel("Save to file or dir: ");

		// panel for grouping buttons and thread chooser
		JPanel buttonpanel = new JPanel();
		buttonpanel.add(downloadButton);
		buttonpanel.add(clearButton);
	
		// position the components
		JPanel toppanel = new JPanel( );
		toppanel.setLayout( new GridBagLayout() );
		GridBagConstraints gc = new GridBagConstraints( );
		// spacing around elements
		gc.insets = new Insets(1,1,0,0);
		// row 0: image, label, urlfield, queryButton
		gc.gridy = 0; gc.weightx = 0.5; gc.weighty = 0.5;
		gc.gridwidth = 1;
		gc.gridheight = 3;
		gc.gridx = 0;  gc.fill = GridBagConstraints.NONE;
//		toppanel.add( new JLabel(image), gc );
		gc.gridheight = 1; 
		gc.gridx = 1;  gc.fill = GridBagConstraints.NONE;
		toppanel.add( urlLabel, gc );
		gc.gridx = 2;  gc.fill = GridBagConstraints.HORIZONTAL;
		toppanel.add( urlField, gc );
		gc.gridx = 3;  gc.fill = GridBagConstraints.HORIZONTAL;
		toppanel.add( queryButton, gc );
		
		// row 1: image, label, outLabel, outputFileField, browseButton
		gc.gridy = 1;
		gc.gridx = 1;  gc.fill = GridBagConstraints.NONE;
		toppanel.add( outLabel, gc );
		gc.gridx = 2;  gc.fill = GridBagConstraints.HORIZONTAL;
		toppanel.add( outputFileField, gc );
		gc.gridx = 3;  gc.fill = GridBagConstraints.HORIZONTAL;
		toppanel.add( browseButton, gc );
		
		// row 2: image, buttons (span 2 columns)
		gc.gridy = 2;
		gc.gridx = 1;  gc.gridwidth = 3; gc.fill = GridBagConstraints.NONE;
		toppanel.add( buttonpanel, gc );
		
		toppanel.setBorder( new SoftBevelBorder(SoftBevelBorder.RAISED) );
		
		// output message area
		Font font = new Font("Monospaced", Font.PLAIN, 12);
		messageArea = new JTextArea(ROWS, COLS);
		messageArea.setBackground( Color.WHITE );
		messageArea.setForeground( Color.BLUE );
		messageArea.setFont( font );
		messageArea.setEditable( false );
		messageArea.setLineWrap( true );
		messageArea.setText( STARTMESSAGE );
		JScrollPane messagePane = new JScrollPane( messageArea,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		messagePane.setAutoscrolls(true);
		messagePane.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED) );

		// add components to container
		Container contents = this.getContentPane();
		contents.add( toppanel, BorderLayout.NORTH );
		contents.add( messagePane, BorderLayout.CENTER );
		contents.add(this.progressBar,BorderLayout.SOUTH );
		contents.setBackground( Color.DARK_GRAY );
	}
	
	/** Add a message to bottom of message area without deleting existing messages.
	 * @param message is text string to append to message area
	 */
	public void addMessage( String message ) {
		messageArea.append( message );
	}
	
	/** run the UI */
	public void run() {
		pack();
		setVisible(true);
	}

	
	/** save the previously selected output directory for next time */
	private File defaultDirectory = null;
	
	/** 
	 * Open a JFileChooser "save" dialog and return the file selected.
	 * @return the file the user selected, or null if he cancelled.
	 * @postcondition updates the defaultDirectory attribute to selected dir.
	 */
	private File getOutputFile( String defaultFilename ) {
		JFileChooser fc = new JFileChooser("Select output file");
		// if currentDir is not null then set the starting
		// directory of JFileChooser.
		// Its nice if the application remembers this,
		// so the next time you display a JFileChooser it
		// starts at the same directory.
		if ( defaultDirectory != null ) fc.setCurrentDirectory( defaultDirectory );
		if ( defaultFilename != null ) fc.setSelectedFile( new File(defaultFilename) );
		
		int result = fc.showSaveDialog( this );
		if ( result == JFileChooser.APPROVE_OPTION ) {
			// save directory for next time
            defaultDirectory = fc.getCurrentDirectory();
            return fc.getSelectedFile();
        } else return null; // user cancelled.
	}
	
	/**
	 * Get the URL from the URL input field.
	 * Verify the string and convert to a URL.
	 * @return a URL for string in the url input field
	 */
	private URL getUrl() {
		String urlname = urlField.getText().trim();
		if ( urlname.isEmpty() ) {
			JOptionPane.showMessageDialog(this, "Please enter the URL to download first",
					"Download what?", JOptionPane.PLAIN_MESSAGE );
			return null;
		}
		if ( ! urlname.matches("\\w+://\\S*[:\\d+]?/\\S+") ) {
			JOptionPane.showMessageDialog(this, 
					"Please enter a URL of the form:\nPROTOCOL://hostname/pathname\n"
					+"Example:  http://www.download.com/pub/bigfile.zip");
			return null;
		}
		
		URL url = null;
		try {
			url = new URL(urlname);
		} catch (MalformedURLException e) {
			JOptionPane.showMessageDialog(this, "Invalid URL", "Invalid URL", JOptionPane.ERROR_MESSAGE);
		}
		return url;
	}

	/**
	 * Download a file from a URL.
	 */	
	class DownloadAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		public DownloadAction() {
			super(DOWNLOADCOMMAND);
		}
		/* @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent evt) {

			URL url = getUrl(); // get value from URL input field
			if ( url == null ) return;

			String outfilename = outputFileField.getText().trim();
			File file = new File( outfilename );
			if ( file.exists() && ! file.isDirectory() ) {
            	int ok = JOptionPane.showConfirmDialog( DownloaderUI.this,
            			file.getPath()+" already exists.  OK to replace?",
            			"Confirm Overwrite", JOptionPane.OK_CANCEL_OPTION );
            	if ( ok != JOptionPane.OK_OPTION ) return;
            }
			URLReader reader = null;
			try {
				reader = new URLReader( url, file,DownloaderUI.this );
			} catch (IOException e) {
				JOptionPane.showMessageDialog(DownloaderUI.this, e.getMessage(), "Download failed", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			if ( reader.getSize() < 0 ) {
				messageArea.append("\nNothing to get.  Bad URL?\n");
				return;
			}
//TODO Fix this so it doesn't freeze the UI during download
			// download the URL
			//int bytes = reader.readURL(); // download the file;
			
			reader.execute();
			
		}
	}
	
	/**
	 * Browse and select an output file or directory.
	 */
	class BrowseAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		public BrowseAction() {
			super(BROWSECOMMAND);
		}
		public void actionPerformed(ActionEvent e) {
			// choose default output filename
			URL url = getUrl();
			String filename = (url==null)? "" : URLUtils.getURLFilename(url);
			// get the output file
			File file = getOutputFile( filename );
			if ( file != null ) outputFileField.setText( file.getPath() );
		}
	}
	/**
	 * Query the URL and print some statistics.
	 */
	class QueryAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		public QueryAction() {
			super(QUERYCOMMAND);
		}
		public void actionPerformed(ActionEvent e) {
			// choose default output filename
			URL url = getUrl();
			if (url == null) return;
			messageArea.setText( URLUtils.queryURL(url) );
		}
	}
	/**
	 * Clear the input and output fields.
	 */
	class ClearAction extends AbstractAction {
		private static final long serialVersionUID = 1L;
		public ClearAction() { super(CLEARCOMMAND); }
		public void actionPerformed(ActionEvent e) {
				urlField.setText("");
				outputFileField.setText("");
				messageArea.setText("");			
		}
	}
}
